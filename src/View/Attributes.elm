module View.Attributes exposing (..)


type alias Attribute c =
    c -> c



{- Function to set the width attribute of CalcConfig -}


width : Float -> Attribute { c | width : Float }
width f =
    \cc ->
        { cc | width = f }



{- Function to set the height attribute of CalcConfig -}


height : Float -> Attribute { c | height : Float }
height f =
    \cc ->
        { cc | height = f }


shape : String -> Attribute { c | shape : String }
shape shp =
    \cc ->
        { cc | shape = shp }

buttonColor : String -> Attribute { c | buttonColor : String }
buttonColor buttonclr =
    \cc ->
        { cc | buttonColor = buttonclr }

rx : String -> Attribute { c | rx : String }
rx shp =
    \cc ->
        { cc | rx = shp }

padding : String -> Attribute { c | padding : String }
padding pd =
    \cc ->
        { cc | padding = pd }

font : String -> Attribute { c | font : String }
font ft =
    \cc ->
        { cc | font = ft }

columns : Int -> Attribute { c | columns : Int }
columns cl =
    \cc ->
        { cc | columns = cl }
